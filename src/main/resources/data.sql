INSERT INTO Gledalac (korisnickoime, lozinka, ime, prezime, kontakttelefon, email, datumrodjenja, uloga, aktivan)
VALUES('ugar', 'ugar', 'stefan' , 'ugarkovic', '0606369998', 'a@a.com', '29-08-1998',  'gledalac', 0);

INSERT INTO Menadzer (korisnickoime, lozinka, ime, prezime, kontakttelefon, email, datumrodjenja, uloga, aktivan)
VALUES('menadzer', 'menadzer', 'stefan' , 'ugarkovic', '0606369998', 'as@a.com', '29-08-1958',  'menadzer', 0);

INSERT INTO Menadzer (korisnickoime, lozinka, ime, prezime, kontakttelefon, email, datumrodjenja, uloga, aktivan)
VALUES('menadzer1', 'menadzer', 'milan' , 'ugarkovic', '0606369998', 'am@a.com', '29-08-1968',  'menadzer', 0);

INSERT INTO Menadzer (korisnickoime, lozinka, ime, prezime, kontakttelefon, email, datumrodjenja, uloga, aktivan)
VALUES('menadzer2', 'menadzer', 'milica' , 'ugarkovic', '0606369998', 'ami@a.com', '29-08-1988',  'menadzer', 0);

INSERT INTO Administrator (korisnickoime, lozinka, ime, prezime, kontakttelefon, email, datumrodjenja, uloga, aktivan)
VALUES('admin', 'admin', 'stefan' , 'ugarkovic', '0606369998', 'a@a.com', '29-08-19998',  'admin', 0);

INSERT INTO FILM (naziv, opis, zanr, trajanje, ocena)
VALUES('Bilo jednom u Holivudu','h','komedija','a',1);

INSERT INTO FILM (naziv, opis, zanr, trajanje, ocena)
VALUES('1917','b','akcija','b',2);

INSERT INTO FILM (naziv, opis, zanr, trajanje, ocena)
VALUES('Dzoker','c','drama','c',3);

INSERT INTO FILM (naziv, opis, zanr, trajanje, ocena)
VALUES('Parazit','d','komedija','d',4);

INSERT INTO BIOSKOP(naziv, adresa, telefon, email, deleted)
VALUES('Bioskop1','a','0606369998','as@',0);

INSERT INTO BIOSKOP(naziv, adresa, telefon, email, deleted)
VALUES('Bioskop2','b','0606369998','as@',0);

INSERT INTO SALA(kapacitet, oznaka, bioskop_id, deleted)
VALUES(10,'a1',1,0);

INSERT INTO SALA(kapacitet, oznaka, bioskop_id, deleted)
VALUES(10,'a2',1,0);

INSERT INTO SALA(kapacitet, oznaka, bioskop_id, deleted)
VALUES(10,'b1',2,0);

INSERT INTO SALA(kapacitet, oznaka, bioskop_id, deleted)
VALUES(10,'b2',2,0);

INSERT INTO PROJEKCIJA(vreme, brrezkarata, cena, film_id, sala_id)
VALUES('2020-09-20T12:00:00', 0, 100, 1, 1);

INSERT INTO PROJEKCIJA(vreme, brrezkarata, cena, film_id, sala_id)
VALUES('2020-09-20T14:00:00', 0, 100, 2, 2);

INSERT INTO PROJEKCIJA(vreme, brrezkarata, cena, film_id, sala_id)
VALUES('2020-09-20T16:00:00', 0, 100, 3, 3);

INSERT INTO PROJEKCIJA(vreme, brrezkarata, cena, film_id, sala_id)
VALUES('2020-09-20T18:00:00', 0, 100, 4, 4);

INSERT INTO PROJEKCIJA(vreme, brrezkarata, cena, film_id, sala_id)
VALUES('2020-08-20T18:00:00', 0, 100, 1, 3);

INSERT INTO Sale_u_bioskopu(bioskop_id,sala_id)
VALUES(1,1);

INSERT INTO Sale_u_bioskopu(bioskop_id,sala_id)
VALUES(1,2);

INSERT INTO Sale_u_bioskopu(bioskop_id,sala_id)
VALUES(2,3);

INSERT INTO Sale_u_bioskopu(bioskop_id,sala_id)
VALUES(2,4);

INSERT INTO Lista_zaduzenih_bioskopa(menadzer_id,bioskop_id)
VALUES(1,1);

INSERT INTO Lista_zaduzenih_bioskopa(menadzer_id,bioskop_id)
VALUES(1,2);

INSERT INTO Lista_zaduzenih_bioskopa(menadzer_id,bioskop_id)
VALUES(2,1);

INSERT INTO Lista_zaduzenih_bioskopa(menadzer_id,bioskop_id)
VALUES(3,1);

INSERT INTO Menadzeri_u_bioskopu(bioskop_id,menadzer_id)
VALUES(1,1);

INSERT INTO Menadzeri_u_bioskopu(bioskop_id,menadzer_id)
VALUES(2,1);

INSERT INTO Menadzeri_u_bioskopu(bioskop_id,menadzer_id)
VALUES(1,2);

INSERT INTO Menadzeri_u_bioskopu(bioskop_id,menadzer_id)
VALUES(2,2);