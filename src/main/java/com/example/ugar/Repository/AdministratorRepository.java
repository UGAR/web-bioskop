package com.example.ugar.Repository;

import com.example.ugar.Entity.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Long> {
    Administrator findByKorisnickoimeAndLozinka(String korIme, String loznika);
}
