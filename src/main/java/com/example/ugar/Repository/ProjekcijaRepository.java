package com.example.ugar.Repository;

import com.example.ugar.Entity.Projekcija;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjekcijaRepository extends JpaRepository<Projekcija, Long> {
    List<Projekcija> findALLByOrderByFilmNazivAsc();
    List<Projekcija> findALLByOrderByFilmNazivDesc();
    List<Projekcija> findALLByOrderByFilmZanrAsc();
    List<Projekcija> findALLByOrderByFilmZanrDesc();
}
