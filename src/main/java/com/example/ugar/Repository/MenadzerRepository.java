package com.example.ugar.Repository;

import com.example.ugar.Entity.Menadzer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenadzerRepository extends JpaRepository<Menadzer, Long> {
    Menadzer findByKorisnickoimeAndLozinka(String korIme, String loznika);
}
