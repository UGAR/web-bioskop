package com.example.ugar.Repository;

import com.example.ugar.Entity.Bioskop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BioskopRepository extends JpaRepository<Bioskop, Long> {
}
