package com.example.ugar.Repository;

import com.example.ugar.Entity.Sala;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalaRepository extends JpaRepository<Sala, Long> {
    Sala findByOznaka(String oznaka);
}
