package com.example.ugar.Repository;

import com.example.ugar.Entity.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilmRepository extends JpaRepository<Film, Long> {
    List<Film> findALLByOrderByNazivAsc();
    List<Film> findALLByOrderByNazivDesc();
    List<Film> findALLByOrderByOpisAsc();
    List<Film> findALLByOrderByOpisDesc();
    Film findByNaziv(String naziv);
}
