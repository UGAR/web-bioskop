package com.example.ugar.Repository;

import com.example.ugar.Entity.Gledalac;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GledalacRepository extends JpaRepository<Gledalac, Long> {
    Gledalac findByKorisnickoimeAndLozinka(String korIme, String loznika);
}
