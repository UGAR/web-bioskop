package com.example.ugar.Controller;

import com.example.ugar.Entity.*;
import com.example.ugar.Service.AdministratorService;
import com.example.ugar.Service.BioskopService;
import com.example.ugar.Service.MenadzerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class AdminController {

    @Autowired
    private AdministratorService as;

    @Autowired
    private BioskopService bs;

    @Autowired
    private MenadzerService ms;

    public Long bioskopId;

    public Long adminId;

    @GetMapping( value = "/administrator/{id}")
    public String homeAdmin(@PathVariable(name="id") Long id, Model model){
        Administrator a = this.as.findById(id);
        adminId = id;
        if(a.getAktivan() == 1) {
            List<Bioskop> bioskopi = this.bs.findAll();
            List<Bioskop> bioskopi1 = new ArrayList<>();
            for(Bioskop b: bioskopi){
                if(b.getDeleted() == 0){
                    bioskopi1.add(b);
                }
            }
            model.addAttribute("bioskopi",bioskopi1);
            model.addAttribute("admin",a);
            return "admin";
        }
        return "redirect:/login";
    }

    @GetMapping(value = "/izmeni-bioskop/{id}")
    public String izmenaBioskopa(@PathVariable(name="id") Long id, Model model){
        Bioskop b = bs.findOne(id);
        bioskopId = id;
        model.addAttribute("bioskop", b);
        return "izmenibioskop";
    }

    @PostMapping(value = "/izmeni-bioskop")
    public String izmeniBioskop(@Valid @ModelAttribute Bioskop bioskop, Model model) {
        bioskop.setId(bioskopId);
        List<Bioskop> tmp = this.bs.findAll();
        bs.update(bioskop);
        return "redirect:/administrator/"+adminId;
    }

    @GetMapping(value = "/dodaj-bioskop")
    public String dodavanjeBioskopa(Model model){
        model.addAttribute("bioskop", new Bioskop());
        return "dodajbioskop";
    }

    @PostMapping(value = "/dodaj-bioskop")
    public String dodajBioskop(@Valid @ModelAttribute Bioskop bioskop, Model model) {
        List<Bioskop> tmp = bs.findAll();
        for(Bioskop b: tmp){
            if(b.getNaziv().equals(bioskop.getNaziv())){
                return "redirect:/dodaj-bioskop";
            }
        }
        bioskop.setDeleted(0);
        bs.dodajBioskop(bioskop);
        return "redirect:/administrator/"+adminId;
    }

    @GetMapping(value = "/obrisi-bioskop/{id}")
    public String obrisiBioskop(@PathVariable(name="id") Long id, Model model){

        Bioskop b = bs.findOne(id);
        b.setDeleted(1);
        bs.update(b);

        return "redirect:/administrator/"+adminId;
    }

    @GetMapping(value = "/pregled-menadzera/{id}")
    public String pregledMenadzera(@PathVariable(name="id") Long id, Model model){

        Bioskop b = bs.findOne(id);
        bioskopId = id;
        List<Menadzer> m = b.getMenadzeri();

        model.addAttribute("menadzeri",m);
        model.addAttribute("admin",this.as.findById(adminId));

        return "pregledmenadzera";
    }

    @GetMapping(value = "/ukloni-menadzera/{id}")
    public String ukloniMenadzera(@PathVariable(name="id") Long id, Model model){

        Menadzer m = ms.findById(id);
        Bioskop b = bs.findOne(bioskopId);
        List<Menadzer> lista = b.getMenadzeri();
        List<Bioskop> lista1 = m.getLista_bioskopa();
        if(lista.size() > 1) {
            lista.remove(m);
            lista1.remove(b);
            b.setMenadzeri(lista);
            ms.update(m);
            bs.update(b);
        }
        return "redirect:/pregled-menadzera/"+bioskopId;
    }

    @GetMapping(value = "/odjava-a/{id}")
    public String odjava(@PathVariable(name = "id") Long id){
        Administrator a = this.as.findById(id);
        a.setAktivan(0);
        as.update(a);
        return "redirect:/";
    }

    @GetMapping(value = "/dodaj-menadzera")
    public String registerFormaMenadzer(Model model){
        model.addAttribute("menadzer", new Menadzer());
        return "dodajmenadzera";
    }

    @PostMapping(value = "/dodaj-menadzera")
    public String register(@Valid @ModelAttribute Menadzer menadzer, Model model) {
        Bioskop b = this.bs.findOne(bioskopId);
        List<Bioskop> lista = menadzer.getLista_bioskopa();
        List<Menadzer> lista1 = b.getMenadzeri();
        List<Menadzer> lista3 = ms.findAll();
        for(Menadzer me: lista3){
            if (me.getKorisnickoime().equals(menadzer.getKorisnickoime())) {
                return "redirect:/dodaj-menadzera";
            }
        }
        lista1.add(menadzer);
        lista.add(b);
        menadzer.setLista_bioskopa(lista);
        menadzer.setAktivan(0);
        ms.registracija(menadzer);
        bs.update(b);
        return "redirect:/pregled-menadzera/"+bioskopId;
    }
}
