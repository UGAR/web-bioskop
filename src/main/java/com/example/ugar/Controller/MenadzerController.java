package com.example.ugar.Controller;

import com.example.ugar.Entity.*;
import com.example.ugar.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
public class MenadzerController {

    @Autowired
    private MenadzerService ms;

    @Autowired
    private BioskopService bs;

    @Autowired
    private SalaService ss;

    @Autowired
    private FilmService fs;

    @Autowired
    private ProjekcijaService ps;

    public Long menadzerId;

    public Long bioskopId;

    public Long salaId;

    public Long projekcijaId;

    @GetMapping(value = "/menadzer/{id}")
    public String homeMenadzer(@PathVariable(name="id") Long id, Model model){

        menadzerId = id;
        Menadzer m = ms.findById(id);
        if(m.getAktivan() == 1) {
            List<Bioskop> lista = m.getLista_bioskopa();
            List<Bioskop> lista1 = new ArrayList<>();
            for(Bioskop b: lista){
                if(b.getDeleted() == 0){
                    lista1.add(b);
                }
            }

            model.addAttribute("bioskopi", lista1);
            model.addAttribute("menadzer", this.ms.findById(id));

            return "menadzer";
        }
        return "redirect:/login";
    }

    @GetMapping(value = "/bioskop/{id}")
    public String pregledBioskopa(@PathVariable(name="id") Long id, Model model){

        bioskopId = id;
        Bioskop b = bs.findOne(id);
        List<Projekcija> projekcije = this.ps.findAll();
        List<Projekcija> projekcije1 = new ArrayList<>();
        List<Sala> sale = new ArrayList<Sala>(bs.findOne(b.getId()).getSale());
        List<Sala> sale1 = new ArrayList<>();
        for(Sala s: sale){
            if(s.getDeleted() == 0 && s.getBioskop().getDeleted() == 0){
                sale1.add(s);
            }
        }
        for(Projekcija p: projekcije){
            if(p.getSala().getBioskop() == b && p.getSala().getDeleted() == 0 && p.getSala().getBioskop().getDeleted() == 0){
                projekcije1.add(p);
            }
        }

        model.addAttribute("sale", sale1);
        model.addAttribute("filmovi", projekcije1);
        model.addAttribute("menadzer", this.ms.findById(menadzerId));

        return "sale";
    }

    @GetMapping(value = "/dodaj-salu")
    public String dodavanjeSale(Model model){
        model.addAttribute("sala", new Sala());
        return "dodajsalu";
    }

    @PostMapping(value = "/dodaj-salu")
    public String dodajSalu(@Valid @ModelAttribute Sala sala, Model model) {
        Bioskop b = bs.findOne(bioskopId);
        sala.setDeleted(0);
        List<Sala> lista = b.getSale();
        for(Sala s: lista){
            if(s.getOznaka().equals(sala.getOznaka())){
                System.out.println(s.getOznaka());
                System.out.println(sala.getOznaka());
                return "redirect:/dodaj-salu";
            }
        }
        lista.add(sala);
        b.setSale(lista);
        sala.setBioskop(b);
        this.ss.dodajSalu(sala);
        bs.update(b);
        return "redirect:/bioskop/"+bioskopId;
    }

    @GetMapping(value = "/izmeni-salu/{id}")
    public String izmenaSale(@PathVariable(name="id") Long id, Model model){
        Sala s = ss.findOne(id);
        salaId = id;
        model.addAttribute("sala", s);
        return "izmenisalu";
    }

    @PostMapping(value = "/izmeni-salu")
    public String izmeniSalu(@Valid @ModelAttribute Sala sala, Model model) {
        Bioskop b = bs.findOne(bioskopId);
        sala.setId(salaId);
        sala.setBioskop(b);
        sala.setDeleted(0);
        ss.update(sala);
        return "redirect:/bioskop/"+bioskopId;
    }

    @GetMapping(value = "/obrisi-salu/{id}")
    public String obrisiSalu(@PathVariable(name="id") Long id, Model model){

        Bioskop b = bs.findOne(bioskopId);
        Sala s = ss.findOne(id);
        s.setDeleted(1);
        ss.update(s);
        model.addAttribute("menadzer", this.ms.findById(menadzerId));

        return "redirect:/bioskop/"+bioskopId;
    }

    @GetMapping(value = "/odjava-m/{id}")
    public String odjava(@PathVariable(name = "id") Long id){
        Menadzer m = this.ms.findById(id);
        m.setAktivan(0);
        ms.update(m);
        return "redirect:/";
    }

    @GetMapping(value = "/dodaj-projekciju")
    public String dodavanjeProjekcije(Model model){
        model.addAttribute("projekcija", new Pretraga());
        return "dodajprojekciju";
    }

    @PostMapping(value = "/dodaj-projekciju")
    public String dodajProjekciju(@Valid @ModelAttribute Pretraga projekcija, Model model) {
        String niz = projekcija.getTekst();
        List<String> myList = new ArrayList<String>(Arrays.asList(niz.split(",")));
        String vreme, cena, sala, film;
        vreme = myList.get(0);
        cena = myList.get(1);
        sala = myList.get(2);
        film = myList.get(3);
        System.out.println(vreme);
        System.out.println(cena);
        System.out.println(sala);
        System.out.println(film);
        List<Projekcija> tmp = this.ps.findAll();
        if(provera(sala, film, cena) == true) {
            Projekcija p = new Projekcija();
            Sala s = this.ss.findByOznaka(sala);
            System.out.println("Vrednost polja deleted: "+s.getDeleted());
            if(s.getDeleted() == 0) {
                System.out.println("Usao sam u if");
                for(Projekcija pr: tmp){
                    if(!pr.getVreme().equals(LocalDateTime.parse(vreme)) && LocalDateTime.parse(vreme).isAfter(LocalDateTime.now())){
                        System.out.println("Sala pr:"+pr.getVreme());
                        System.out.println("Sala s:"+LocalDateTime.parse(vreme));
                        Film f = this.fs.findByNaziv(film);
                        p.setVreme(LocalDateTime.parse(vreme));
                        p.setCena(Double.parseDouble(cena));
                        p.setSala(s);
                        p.setFilm(f);
                        p.setBrRezKarata(0);
                        this.ps.dodajProjekciju(p);
                        return "redirect:/bioskop/" + bioskopId;
                    }
                }
            }
        }

        return "redirect:/dodaj-projekciju";
    }

    public Boolean provera(String sala, String film, String cena){
        if(this.ss.findByOznaka(sala) == null || this.fs.findByNaziv(film) == null || !cena.matches("[0-9]+")){
            return false;
        }
        return true;
    }

    @GetMapping(value = "/izmeni-projekciju/{id}")
    public String izmenaProjekcije(@PathVariable(name="id") Long id, Model model){
        projekcijaId = id;
        Projekcija p = ps.findById(id);
        model.addAttribute("projekcija", p);
        return "izmeniprojekciju";
    }

    /*@PostMapping(value = "/izmeni-projekciju")
    public String izmeniProjekciju(@Valid @ModelAttribute Projekcija projekcija, Model model) {
        projekcija.setId(projekcijaId);
        ps.update(projekcija);
        return "redirect:/bioskop/"+bioskopId;
    }*/
}
