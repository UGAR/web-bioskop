package com.example.ugar.Controller;

import com.example.ugar.Entity.*;
import com.example.ugar.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class GledalacController {

    @Autowired
    private FilmService fs;

    @Autowired
    private BioskopService bs;

    @Autowired
    private SalaService ss;

    @Autowired
    private GledalacService gs;

    @Autowired
    private ProjekcijaService ps;

    public Long gledalacId;

    @GetMapping( value = "/gledalac/{id}")
    public String homeGledalac(@PathVariable(name="id") Long id, Model model){
        Gledalac g = this.gs.findById(id);
        if(g.getAktivan() == 1) {
            pregledProfila(id, model);
            gledalacId = id;

            List<Projekcija> projekcije = this.ps.findAll();
            List<Projekcija> projekcije1 = new ArrayList<>();
            for (Projekcija p : projekcije) {
                if (p.getSala().getDeleted() == 0 && p.getSala().getBioskop().getDeleted() == 0) {
                    projekcije1.add(p);
                }
            }

            model.addAttribute("filmovi", projekcije1);
            model.addAttribute("pretraga", new Pretraga());
            model.addAttribute("gledalac", this.gs.findById(id));
            return "gledalac";
        }
        return "redirect:/login";
    }


    @GetMapping(value = "/profil/{id}")
    public String pregledProfila(@PathVariable(name = "id") Long id, Model model){
        System.out.println(id);
        Gledalac g = this.gs.findById(id);
        List<Film> lista = g.getLista_odgledanih_filmova();
        model.addAttribute("gledalac", this.gs.findById(id));
        model.addAttribute("filmovi", lista);
        return "profil";
    }

    @PostMapping("/pretrazi-1")
    public String pretraga(@ModelAttribute Pretraga p, Model model) {

        String pretraga = p.getTekst();

        List<Projekcija> projekcije = this.ps.findAll();
        List<Projekcija> tmp1 = new ArrayList<>();
        for(Projekcija pr: projekcije){
            if(pr.getSala().getDeleted() == 0 && pr.getSala().getBioskop().getDeleted() == 0) {
                if (pr.getFilm().getNaziv().toUpperCase().contains(pretraga.toUpperCase())
                        || pr.getFilm().getZanr().toUpperCase().contains(pretraga.toUpperCase())
                        || pr.getFilm().getOpis().toUpperCase().contains(pretraga.toUpperCase())
                        || pr.getFilm().getOcena().toString().toUpperCase().contains(pretraga.toUpperCase())
                        || pr.getFilm().getTrajanje().toUpperCase().contains(pretraga.toUpperCase())) {

                    tmp1.add(pr);
                }
            }
        }

        model.addAttribute("filmovi", tmp1);
        model.addAttribute("pretraga", new Pretraga());
        model.addAttribute("gledalac", this.gs.findById(gledalacId));
        return "gledalac";

    }

    @PostMapping("/pretrazi-1-2")
    public String visekriterijumskaPretraga(@ModelAttribute Pretraga pretraga, Model model) {

        String p1 = pretraga.getTekst();
        List<String> myList = new ArrayList<String>(Arrays.asList(p1.split(",")));
        String s1 = "", s2 = "";
        System.out.println(myList);
        s1 = myList.get(0);
        s2 = myList.get(1);
        System.out.println(s1);
        System.out.println(s2);

        List<Projekcija> projekcije = this.ps.findAll();
        List<Projekcija> tmp1 = new ArrayList<>();
        for(Projekcija pr: projekcije){
            if(pr.getSala().getDeleted() == 0 && pr.getSala().getBioskop().getDeleted() == 0) {
                if (pr.getFilm().getNaziv().toUpperCase().contains(s1.toUpperCase())
                        || pr.getFilm().getZanr().toUpperCase().contains(s2.toUpperCase())) {
                    tmp1.add(pr);
                }
            }
        }

        model.addAttribute("filmovi", tmp1);
        model.addAttribute("pretraga", new Pretraga());
        model.addAttribute("gledalac", this.gs.findById(gledalacId));
        return "gledalac";

    }

    @GetMapping("/sort/naziv-rastuce-1")
    public String sortirajPoNazivuRastuce( Model model) {

        List<Projekcija> projekcije = this.ps.findAll();
        List<Projekcija> projekcije1 = new ArrayList<>();

        projekcije = this.ps.sortByNazivRastuce();
        for(Projekcija p: projekcije){
            if(p.getSala().getDeleted() == 0 && p.getSala().getBioskop().getDeleted() == 0){
                projekcije1.add(p);
            }
        }

        model.addAttribute("filmovi", projekcije1);
        model.addAttribute("pretraga", new Pretraga());
        model.addAttribute("gledalac", this.gs.findById(gledalacId));
        return "gledalac";
    }

    @GetMapping("/sort/naziv-opadajuce-1")
    public String sortirajPoNazivuOpadajuce( Model model) {

        List<Projekcija> projekcije = this.ps.findAll();
        List<Projekcija> projekcije1 = new ArrayList<>();

        projekcije = this.ps.sortByNazivOpadajuce();
        for(Projekcija p: projekcije){
            if(p.getSala().getDeleted() == 0 && p.getSala().getBioskop().getDeleted() == 0){
                projekcije1.add(p);
            }
        }

        model.addAttribute("filmovi", projekcije1);
        model.addAttribute("pretraga", new Pretraga());
        model.addAttribute("gledalac", this.gs.findById(gledalacId));
        return "gledalac";
    }

    @GetMapping("/sort/zanr-rastuce-1")
    public String sortirajPoZanruRastuce( Model model) {

        List<Projekcija> projekcije = this.ps.findAll();
        List<Projekcija> projekcije1 = new ArrayList<>();

        projekcije = this.ps.sortByZanrRastuce();
        for(Projekcija p: projekcije){
            if(p.getSala().getDeleted() == 0 && p.getSala().getBioskop().getDeleted() == 0) {
                projekcije1.add(p);
            }
        }

        model.addAttribute("filmovi", projekcije1);
        model.addAttribute("pretraga", new Pretraga());
        model.addAttribute("gledalac", this.gs.findById(gledalacId));
        return "gledalac";
    }

    @GetMapping("/sort/zanr-opadajuce-1")
    public String sortirajPoZanruOpadajuce( Model model) {

        List<Projekcija> projekcije = this.ps.findAll();
        List<Projekcija> projekcije1 = new ArrayList<>();

        projekcije = this.ps.sortByZanrOpadajuce();
        for(Projekcija p: projekcije){
            if(p.getSala().getDeleted() == 0 && p.getSala().getBioskop().getDeleted() == 0){
                projekcije1.add(p);
            }
        }

        model.addAttribute("filmovi", projekcije1);
        model.addAttribute("pretraga", new Pretraga());
        model.addAttribute("gledalac", this.gs.findById(gledalacId));
        return "gledalac";
    }

    @GetMapping(value = "/rezervacija/{id}")
    public String rezervisiFilm(@PathVariable(name = "id") Long id, Model model) {
        System.out.println("Id projekcije: "+id);
        Gledalac g = this.gs.findById(gledalacId);
        Projekcija p = this.ps.findById(id);
        if(p.getSala().getKapacitet() > p.getBrRezKarata() && p.getVreme().isAfter(LocalDateTime.now())) {
            p.setBrRezKarata(p.getBrRezKarata()+1);
            List<Projekcija> lista = g.getLista_rez_karata();
            lista.add(p);
            g.setLista_rez_karata(lista);
            gs.update(g);
            ps.update(p);
        }
        return "redirect:/gledalac/"+gledalacId;
    }

    @GetMapping(value = "/gledalac/rezervacija/{id}")
    public String rezervisaniFilmovi(@PathVariable(name = "id") Long id, Model model) {
        Gledalac g = this.gs.findById(gledalacId);
        List<Projekcija> lista = g.getLista_rez_karata();
        model.addAttribute("filmovi", lista);
        model.addAttribute("gledalac", this.gs.findById(gledalacId));
        return "rezervacija";
    }

    @GetMapping(value = "/potvrdi/{id}")
    public String potvrdiRezervaciju(@PathVariable(name = "id") Long id, Model model) {
        Projekcija p = this.ps.findById(id);
        Gledalac g = this.gs.findById(gledalacId);
        List<Film> lista = g.getLista_odgledanih_filmova();
        List<Projekcija> lista1 = g.getLista_rez_karata();
        if(p.getVreme().isAfter(LocalDateTime.now())) {
            lista.add(p.getFilm());
            g.setLista_odgledanih_filmova(lista);
            System.out.println("Pre brisanja: " + lista1.size());
            lista1.remove(p);
            System.out.println("Posle brisanja: " + lista1.size());
            g.setLista_rez_karata(lista1);
            gs.update(g);
        }
        return "redirect:/gledalac/rezervacija/"+gledalacId;
    }

    @GetMapping(value = "/odustani/{id}")
    public String otkaziRezervaciju(@PathVariable(name = "id") Long id, Model model) {
        Projekcija p = this.ps.findById(id);
        Gledalac g = this.gs.findById(gledalacId);
        List<Projekcija> lista1 = g.getLista_rez_karata();
        lista1.remove(p);
        g.setLista_rez_karata(lista1);
        p.setBrRezKarata(p.getBrRezKarata()-1);
        ps.update(p);
        gs.update(g);

        return "redirect:/gledalac/rezervacija/"+gledalacId;
    }

    @GetMapping(value = "/odjava/{id}")
    public String odjava(@PathVariable(name = "id") Long id){
        Gledalac g = this.gs.findById(id);
        g.setAktivan(0);
        gs.update(g);
        return "redirect:/";
    }

}
