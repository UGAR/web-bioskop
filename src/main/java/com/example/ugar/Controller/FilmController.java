package com.example.ugar.Controller;

import com.example.ugar.Entity.*;
import com.example.ugar.Service.BioskopService;
import com.example.ugar.Service.FilmService;
import com.example.ugar.Service.ProjekcijaService;
import com.example.ugar.Service.SalaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class FilmController {

    @Autowired
    private FilmService fs;

    @Autowired
    private SalaService ss;

    @Autowired
    private BioskopService bs;

    @Autowired
    private ProjekcijaService ps;

    @GetMapping( value = "/")
    public String home(Model model){
        List<Projekcija> projekcije = this.ps.findAll();
        List<Projekcija> projekcije1 = new ArrayList<>();
        for(Projekcija p: projekcije){
            if(p.getSala().getDeleted() == 0 && p.getSala().getBioskop().getDeleted() == 0){
                projekcije1.add(p);
            }
        }

        model.addAttribute("filmovi", projekcije1);
        model.addAttribute("pretraga", new Pretraga());
        return "home";
    }

    @PostMapping("/pretrazi")
    public String pretraga(@ModelAttribute Pretraga p, Model model) {

        String pretraga = p.getTekst();

        List<Projekcija> projekcije = this.ps.findAll();
        List<Projekcija> tmp1 = new ArrayList<>();
        for(Projekcija pr: projekcije){
            if(pr.getSala().getDeleted() == 0 && pr.getSala().getBioskop().getDeleted() == 0) {
                if (pr.getFilm().getNaziv().toUpperCase().contains(pretraga.toUpperCase())
                        || pr.getFilm().getZanr().toUpperCase().contains(pretraga.toUpperCase())
                        || pr.getFilm().getOpis().toUpperCase().contains(pretraga.toUpperCase())
                        || pr.getFilm().getOcena().toString().toUpperCase().contains(pretraga.toUpperCase())
                        || pr.getFilm().getTrajanje().toUpperCase().contains(pretraga.toUpperCase())) {

                    tmp1.add(pr);
                }
            }
        }

        model.addAttribute("filmovi", tmp1);
        model.addAttribute("pretraga", new Pretraga());
        return "home";

    }

    @PostMapping("/pretrazi-2")
    public String visekriterijumskaPretraga(@ModelAttribute Pretraga pretraga, Model model) {

        String p1 = pretraga.getTekst();
        List<String> myList = new ArrayList<String>(Arrays.asList(p1.split(",")));
        String s1 = "", s2 = "";
        System.out.println(myList);
        s1 = myList.get(0);
        s2 = myList.get(1);
        System.out.println(s1);
        System.out.println(s2);

        List<Projekcija> projekcije = this.ps.findAll();
        List<Projekcija> tmp1 = new ArrayList<>();
        for(Projekcija pr: projekcije){
            if(pr.getSala().getDeleted() == 0 && pr.getSala().getBioskop().getDeleted() == 0) {
                if (pr.getFilm().getNaziv().toUpperCase().contains(s1.toUpperCase())
                        || pr.getFilm().getZanr().toUpperCase().contains(s2.toUpperCase())) {
                    tmp1.add(pr);
                }
            }
        }

        model.addAttribute("filmovi", tmp1);
        model.addAttribute("pretraga", new Pretraga());
        return "home";

    }

    @GetMapping("/sort/naziv-rastuce")
    public String sortirajPoNazivuRastuce( Model model) {


        List<Projekcija> projekcije = this.ps.findAll();
        List<Projekcija> projekcije1 = new ArrayList<>();

        projekcije = this.ps.sortByNazivRastuce();
        for(Projekcija p: projekcije){
            if(p.getSala().getDeleted() == 0 && p.getSala().getBioskop().getDeleted() == 0){
                projekcije1.add(p);
            }
        }

        model.addAttribute("filmovi", projekcije1);
        model.addAttribute("pretraga", new Pretraga());
        return "home";
    }

    @GetMapping("/sort/naziv-opadajuce")
    public String sortirajPoNazivuOpadajuce( Model model) {

        List<Projekcija> projekcije = this.ps.findAll();
        List<Projekcija> projekcije1 = new ArrayList<>();

        projekcije = this.ps.sortByNazivOpadajuce();
        for(Projekcija p: projekcije){
            if(p.getSala().getDeleted() == 0 && p.getSala().getBioskop().getDeleted() == 0){
                projekcije1.add(p);
            }
        }

        model.addAttribute("filmovi", projekcije1);
        model.addAttribute("pretraga", new Pretraga());
        return "home";
    }

    @GetMapping("/sort/zanr-rastuce")
    public String sortirajPoZanruRastuce( Model model) {

        List<Projekcija> projekcije = this.ps.findAll();
        List<Projekcija> projekcije1 = new ArrayList<>();

        projekcije = this.ps.sortByZanrRastuce();
        for(Projekcija p: projekcije){
            if(p.getSala().getDeleted() == 0 && p.getSala().getBioskop().getDeleted() == 0){
                projekcije1.add(p);
            }
        }

        model.addAttribute("filmovi", projekcije1);
        model.addAttribute("pretraga", new Pretraga());
        return "home";
    }

    @GetMapping("/sort/zanr-opadajuce")
    public String sortirajPoZanruOpadajuce( Model model) {

        List<Projekcija> projekcije = this.ps.findAll();
        List<Projekcija> projekcije1 = new ArrayList<>();

        projekcije = this.ps.sortByZanrOpadajuce();
        for(Projekcija p: projekcije){
            if(p.getSala().getDeleted() == 0 && p.getSala().getBioskop().getDeleted() == 0){
                projekcije1.add(p);
            }
        }

        model.addAttribute("filmovi", projekcije1);
        model.addAttribute("pretraga", new Pretraga());
        return "home";
    }

}
