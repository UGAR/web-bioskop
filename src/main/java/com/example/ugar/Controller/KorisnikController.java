package com.example.ugar.Controller;

import com.example.ugar.Entity.Administrator;
import com.example.ugar.Entity.Gledalac;
import com.example.ugar.Entity.Korisnik;
import com.example.ugar.Entity.Menadzer;
import com.example.ugar.Service.AdministratorService;
import com.example.ugar.Service.GledalacService;
import com.example.ugar.Service.MenadzerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class KorisnikController {

    @Autowired
    private GledalacService gs;

    @Autowired
    private MenadzerService ms;

    @Autowired
    private AdministratorService as;

    @GetMapping( "/login")
    public String loginForma(Model model){
        model.addAttribute("korisnik", new Korisnik());

        return "login";
    }

    @PostMapping("/loginn")
    public String login(@Valid @ModelAttribute Korisnik korisnik){
        System.out.println(korisnik.getKorisnickoime());
        System.out.println(korisnik.getLozinka());
        Gledalac gled = this.gs.proveriKorImeILozinku(korisnik.getKorisnickoime(), korisnik.getLozinka());
        Menadzer men = this.ms.proveriKorImeILozinku(korisnik.getKorisnickoime(), korisnik.getLozinka());
        Administrator admin = this.as.proveriKorImeILozinku(korisnik.getKorisnickoime(), korisnik.getLozinka());
        if(gled != null){
            gled.setAktivan(1);
            gs.update(gled);
            return "redirect:/gledalac/" + gled.getId();
        }else if(men != null){
            men.setAktivan(1);
            ms.update(men);
            return "redirect:/menadzer/" + men.getId();
        }else if(admin != null){
            admin.setAktivan(1);
            as.update(admin);
            return "redirect:/administrator/" + admin.getId();
        }

        return "redirect:/login";
    }

    @GetMapping(value = "/register")
    public String registerForma(Model model){
        model.addAttribute("gledalac", new Gledalac());
        return "register";
    }

    @PostMapping(value = "/register")
    public String register(@Valid @ModelAttribute Gledalac gledalac, Model model) {
        List<Gledalac> lista = this.gs.findAll();
        for(Gledalac g : lista){
            if(g.getKorisnickoime().equals(gledalac.getKorisnickoime())){
                return "redirect:/register";
            }
        }
        this.gs.registracija(gledalac);
        return "redirect:/login";
    }
}
