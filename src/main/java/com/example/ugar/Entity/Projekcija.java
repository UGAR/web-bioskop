package com.example.ugar.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Projekcija implements Serializable {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column
    private LocalDateTime vreme;

    @Column
    private int brrezkarata;

    @Column
    private double cena;

    @ManyToOne
    @JoinColumn(name = "film_id")
    private Film film;

    @ManyToOne
    @JoinColumn(name = "sala_id")
    private Sala sala;

    @OneToMany
    @JoinTable(name = "Lista_gledalaca_za_projekciju",
            joinColumns = @JoinColumn(name = "projekcija_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "gledalac_id", referencedColumnName = "id"))
    private List<Gledalac> listaGledalaca = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getVreme() {
        return vreme;
    }

    public void setVreme(LocalDateTime vreme) {
        this.vreme = vreme;
    }

    public int getBrRezKarata() {
        return brrezkarata;
    }

    public void setBrRezKarata(int brrezkarata) {
        this.brrezkarata = brrezkarata;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    public List<Gledalac> getListaGledalaca() {
        return listaGledalaca;
    }

    public void setListaGledalaca(List<Gledalac> listaGledalaca) {
        this.listaGledalaca = listaGledalaca;
    }
}
