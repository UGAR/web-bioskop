package com.example.ugar.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Pretraga implements Serializable {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column
    private String tekst;

    public String getTekst() {
        return tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }
}
