package com.example.ugar.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@DiscriminatorValue("1")
public class Gledalac extends Korisnik {

    @ManyToMany
    @JoinTable(name = "Lista_odgledanih_filmova",
            joinColumns = @JoinColumn(name = "gledalac_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "film_id", referencedColumnName = "id"))
    private List<Film> lista_odgledanih_filmova = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "Lista_rez_karata",
            joinColumns = @JoinColumn(name = "gledalac_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "projekcija_id", referencedColumnName = "id"))
    private List<Projekcija> lista_rez_karata = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "Lista_ocenjenih_filmova",
            joinColumns = @JoinColumn(name = "gledalac_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "ocena", referencedColumnName = "ocena"))
    private List<Film> lista_ocenjenih_filmova = new ArrayList<>();

    public List<Film> getLista_odgledanih_filmova() {
        return lista_odgledanih_filmova;
    }

    public void setLista_odgledanih_filmova(List<Film> lista_odgledanih_filmova) {
        this.lista_odgledanih_filmova = lista_odgledanih_filmova;
    }

    public List<Projekcija> getLista_rez_karata() {
        return lista_rez_karata;
    }

    public void setLista_rez_karata(List<Projekcija> lista_rez_karata) {
        this.lista_rez_karata = lista_rez_karata;
    }

    public List<Film> getLista_ocenjenih_filmova() {
        return lista_ocenjenih_filmova;
    }

    public void setLista_ocenjenih_filmova(List<Film> lista_ocenjenih_filmova) {
        this.lista_ocenjenih_filmova = lista_ocenjenih_filmova;
    }
}
