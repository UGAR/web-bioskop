package com.example.ugar.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Bioskop {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column
    private String naziv;

    @Column
    private String adresa;

    @Column
    private String telefon;

    @Column
    private String email;

    @Column
    private Integer deleted;

    @ManyToMany
    @JoinTable(name = "Menadzeri_u_bioskopu",
            joinColumns = @JoinColumn(name = "bioskop_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "menadzer_id", referencedColumnName = "id"))
    private List<Menadzer> menadzeri = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "Sale_u_bioskopu",
            joinColumns = @JoinColumn(name = "bioskop_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "sala_id", referencedColumnName = "id"))
    private List<Sala> sale = new ArrayList<>();

    public List<Sala> getSale() {
        return sale;
    }

    public void setSale(List<Sala> sale) {
        this.sale = sale;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Menadzer> getMenadzeri() {
        return menadzeri;
    }

    public void setMenadzeri(List<Menadzer> menadzeri) {
        this.menadzeri = menadzeri;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
}
