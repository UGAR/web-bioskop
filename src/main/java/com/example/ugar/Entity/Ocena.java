package com.example.ugar.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Ocena implements Serializable {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column
    private String ocenaFilma;

    public String getOcenaFilma() {
        return ocenaFilma;
    }

    public void setOcenaFilma(String ocenaFilma) {
        this.ocenaFilma = ocenaFilma;
    }
}
