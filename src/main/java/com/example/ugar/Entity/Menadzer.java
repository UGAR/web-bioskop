package com.example.ugar.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@DiscriminatorValue("2")
public class Menadzer extends Korisnik{

    @ManyToMany
    @JoinTable(name = "Lista_zaduzenih_bioskopa",
            joinColumns = @JoinColumn(name = "menadzer_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "bioskop_id", referencedColumnName = "id"))
    private List<Bioskop> lista_bioskopa = new ArrayList<>();

    public List<Bioskop> getLista_bioskopa() {
        return lista_bioskopa;
    }

    public void setLista_bioskopa(List<Bioskop> lista_bioskopa) {
        this.lista_bioskopa = lista_bioskopa;
    }

}
