package com.example.ugar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UgarApplication {

	public static void main(String[] args) {
		SpringApplication.run(UgarApplication.class, args);
	}

}
