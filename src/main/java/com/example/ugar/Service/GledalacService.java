package com.example.ugar.Service;

import com.example.ugar.Entity.Gledalac;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface GledalacService {
    Gledalac proveriKorImeILozinku(String korIme, String loznika);
    Gledalac registracija(Gledalac gledalac);
    Gledalac findById(Long id);
    Gledalac update(Gledalac gledalac);
    List<Gledalac> findAll();
}
