package com.example.ugar.Service;

import com.example.ugar.Entity.Film;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FilmService {
    List<Film> findAll();
    List<Film> sortByNazivRastuce();
    List<Film> sortByNazivOpadajuce();
    List<Film> sortByOpisRastuce();
    List<Film> sortByOpisOpadajuce();
    Film findById(Long id);
    Film update(Film film);
    Film findByNaziv(String naziv);
}
