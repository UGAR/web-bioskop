package com.example.ugar.Service;

import com.example.ugar.Entity.Bioskop;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BioskopService {
    List<Bioskop> findAll();
    Bioskop findOne(Long id);
    Bioskop update(Bioskop bioskop);
    Bioskop dodajBioskop(Bioskop bioskop);
}
