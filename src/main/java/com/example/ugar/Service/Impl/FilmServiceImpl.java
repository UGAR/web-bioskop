package com.example.ugar.Service.Impl;

import com.example.ugar.Entity.Film;
import com.example.ugar.Repository.FilmRepository;
import com.example.ugar.Service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FilmServiceImpl implements FilmService {

    @Autowired
    private FilmRepository fr;


    @Override
    public List<Film> findAll() {
        return this.fr.findAll();
    }

    @Override
    public List<Film> sortByNazivRastuce() {
        return this.fr.findALLByOrderByNazivAsc();
    }

    @Override
    public List<Film> sortByNazivOpadajuce() {
        return this.fr.findALLByOrderByNazivDesc();
    }

    @Override
    public List<Film> sortByOpisRastuce() {
        return this.fr.findALLByOrderByOpisAsc();
    }

    @Override
    public List<Film> sortByOpisOpadajuce() {
        return this.fr.findALLByOrderByOpisDesc();
    }

    @Override
    public Film findById(Long id) {
        return this.fr.getOne(id);
    }

    @Override
    public Film update(Film film){
        Film updateFilm = this.fr.getOne(film.getId());

        return this.fr.save(updateFilm);
    }

    @Override
    public Film findByNaziv(String naziv) {
        return this.fr.findByNaziv(naziv);
    }
}
