package com.example.ugar.Service.Impl;

import com.example.ugar.Entity.Menadzer;
import com.example.ugar.Repository.MenadzerRepository;
import com.example.ugar.Service.MenadzerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenadzerServiceImpl implements MenadzerService {

    @Autowired
    private MenadzerRepository mr;


    @Override
    public Menadzer proveriKorImeILozinku(String korIme, String loznika) {
        Menadzer m = this.mr.findByKorisnickoimeAndLozinka(korIme, loznika);
        if(m == null){
            return null;
        }
        return  m;
    }

    @Override
    public Menadzer findById(Long id) {
        return this.mr.getOne(id);
    }

    @Override
    public Menadzer update(Menadzer menadzer) {
        Menadzer updateMenadzer = this.mr.getOne(menadzer.getId());

        return this.mr.save(updateMenadzer);
    }

    @Override
    public Menadzer registracija(Menadzer menadzer) {
        if (menadzer.getId() != null) {
            return null;
        }

        menadzer.setUloga("menadzer");
        return this.mr.save(menadzer);
    }

    @Override
    public List<Menadzer> findAll() {
        return this.mr.findAll();
    }
}
