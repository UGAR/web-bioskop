package com.example.ugar.Service.Impl;

import com.example.ugar.Entity.Projekcija;
import com.example.ugar.Repository.ProjekcijaRepository;
import com.example.ugar.Service.ProjekcijaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjekcijaServiceImpl implements ProjekcijaService {

    @Autowired
    private ProjekcijaRepository pr;

    @Override
    public List<Projekcija> findAll() {
        return this.pr.findAll();
    }

    @Override
    public List<Projekcija> sortByNazivRastuce() {
        return this.pr.findALLByOrderByFilmNazivAsc();
    }

    @Override
    public List<Projekcija> sortByNazivOpadajuce() {
        return this.pr.findALLByOrderByFilmNazivDesc();
    }

    @Override
    public List<Projekcija> sortByZanrRastuce() {
        return this.pr.findALLByOrderByFilmZanrAsc();
    }

    @Override
    public List<Projekcija> sortByZanrOpadajuce() {
        return this.pr.findALLByOrderByFilmZanrDesc();
    }

    @Override
    public Projekcija findById(Long id) {
        return this.pr.getOne(id);
    }

    @Override
    public Projekcija update(Projekcija projekcija) {
        Projekcija updateProjekcija = this.pr.getOne(projekcija.getId());

        return this.pr.save(updateProjekcija);
    }

    @Override
    public Projekcija dodajProjekciju(Projekcija projekcija) {
        if (projekcija.getId() != null) {
            return null;
        }

        return this.pr.save(projekcija);
    }
}
