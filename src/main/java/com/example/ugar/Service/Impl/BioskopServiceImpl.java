package com.example.ugar.Service.Impl;

import com.example.ugar.Entity.Bioskop;
import com.example.ugar.Repository.BioskopRepository;
import com.example.ugar.Service.BioskopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BioskopServiceImpl implements BioskopService {

    @Autowired
    private BioskopRepository br;

    @Override
    public List<Bioskop> findAll() {
        return this.br.findAll();
    }

    @Override
    public Bioskop findOne(Long id) {
        return this.br.getOne(id);
    }

    @Override
    public Bioskop update(Bioskop bioskop) {
        Bioskop updateBioskop = this.br.getOne(bioskop.getId());

        updateBioskop.setAdresa(bioskop.getAdresa());
        updateBioskop.setEmail(bioskop.getEmail());
        updateBioskop.setNaziv(bioskop.getNaziv());
        updateBioskop.setTelefon(bioskop.getTelefon());
        return this.br.save(updateBioskop);
    }

    @Override
    public Bioskop dodajBioskop(Bioskop bioskop) {
        if (bioskop.getId() != null) {
            return null;
        }

        return this.br.save(bioskop);
    }

}
