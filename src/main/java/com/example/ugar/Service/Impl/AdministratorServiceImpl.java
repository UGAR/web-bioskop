package com.example.ugar.Service.Impl;

import com.example.ugar.Entity.Administrator;
import com.example.ugar.Repository.AdministratorRepository;
import com.example.ugar.Service.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdministratorServiceImpl implements AdministratorService {

    @Autowired
    private AdministratorRepository ar;

    @Override
    public Administrator proveriKorImeILozinku(String korIme, String loznika) {
        Administrator a = this.ar.findByKorisnickoimeAndLozinka(korIme, loznika);
        if(a == null){
            return null;
        }
        return  a;
    }

    @Override
    public Administrator update(Administrator administrator) {
        Administrator updateadmin = this.ar.getOne(administrator.getId());

        return this.ar.save(updateadmin);
    }

    @Override
    public Administrator findById(Long id) {
        return this.ar.getOne(id);
    }
}
