package com.example.ugar.Service.Impl;

import com.example.ugar.Entity.Bioskop;
import com.example.ugar.Entity.Gledalac;
import com.example.ugar.Entity.Sala;
import com.example.ugar.Repository.SalaRepository;
import com.example.ugar.Service.SalaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalaServiceImpl implements SalaService {

    @Autowired
    private SalaRepository sr;


    @Override
    public Sala findOne(Long id) {
        return this.sr.getOne(id);
    }

    @Override
    public Sala update(Sala sala) {
        Sala updateSala = this.sr.getOne(sala.getId());

        updateSala.setOznaka(sala.getOznaka());
        updateSala.setKapacitet(sala.getKapacitet());
        updateSala.setBioskop(sala.getBioskop());
        updateSala.setDeleted(sala.getDeleted());
        return this.sr.save(updateSala);
    }

    @Override
    public Sala dodajSalu(Sala sala) {
        if (sala.getId() != null) {
            return null;
        }

        return this.sr.save(sala);
    }

    @Override
    public void obrisiSalu(Sala sala) {
        this.sr.delete(sala);
    }

    @Override
    public Sala findByOznaka(String sala) {
        return this.sr.findByOznaka(sala);
    }
}
