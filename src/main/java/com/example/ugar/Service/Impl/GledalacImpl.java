package com.example.ugar.Service.Impl;

import com.example.ugar.Entity.Gledalac;
import com.example.ugar.Repository.GledalacRepository;
import com.example.ugar.Service.GledalacService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GledalacImpl implements GledalacService {

    @Autowired
    private GledalacRepository gr;


    @Override
    public Gledalac proveriKorImeILozinku(String korIme, String loznika) {
        Gledalac g = this.gr.findByKorisnickoimeAndLozinka(korIme, loznika);
        if(g == null){
            return null;
        }
        return  g;
    }

    @Override
    public Gledalac registracija(Gledalac gledalac){

        if (gledalac.getId() != null) {
            return null;
        }

        gledalac.setUloga("gledalac");
        return this.gr.save(gledalac);
    }

    @Override
    public Gledalac findById(Long id) {
        return this.gr.getOne(id);
    }

    @Override
    public Gledalac update(Gledalac gledalac){
        Gledalac updateGledalac = this.gr.getOne(gledalac.getId());

        return this.gr.save(updateGledalac);
    }

    @Override
    public List<Gledalac> findAll() {
        return this.gr.findAll();
    }

}
