package com.example.ugar.Service;

import com.example.ugar.Entity.Projekcija;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProjekcijaService {
    List<Projekcija> findAll();
    List<Projekcija> sortByNazivRastuce();
    List<Projekcija> sortByNazivOpadajuce();
    List<Projekcija> sortByZanrRastuce();
    List<Projekcija> sortByZanrOpadajuce();
    Projekcija findById(Long id);
    Projekcija update(Projekcija projekcija);
    Projekcija dodajProjekciju(Projekcija projekcija);
}
