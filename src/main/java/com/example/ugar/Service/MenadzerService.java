package com.example.ugar.Service;

import com.example.ugar.Entity.Menadzer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MenadzerService {
    Menadzer proveriKorImeILozinku(String korIme, String loznika);
    Menadzer findById(Long id);
    Menadzer update(Menadzer menadzer);
    Menadzer registracija(Menadzer menadzer);
    List<Menadzer> findAll();
}
