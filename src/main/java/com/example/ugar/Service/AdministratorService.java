package com.example.ugar.Service;

import com.example.ugar.Entity.Administrator;
import org.springframework.stereotype.Service;

@Service
public interface AdministratorService {
    Administrator proveriKorImeILozinku(String korIme, String loznika);
    Administrator update(Administrator administrator);
    Administrator findById(Long id);
}
