package com.example.ugar.Service;

import com.example.ugar.Entity.Sala;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SalaService {
    Sala findOne(Long id);
    Sala update(Sala sala);
    Sala dodajSalu(Sala sala);
    void obrisiSalu(Sala sala);
    Sala findByOznaka(String sala);
}
